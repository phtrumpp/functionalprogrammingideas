package combinatorpattern;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Function;
import static combinatorpattern.CustomerRegistrationValidator.*;


/**
 * Erbt von Function<T,R> Interface und nimmt als Argument einen Typ Customer und
 * gibt ein Enum ValidationResult zurück.
 *
 * Statische Methoden dienen zur validierung einzelner Customer Attribute.
 * Default Methode "and" ermöglicht das Verketten der einzelnen Validierungsfunktionen
 *
 */
public interface CustomerRegistrationValidator extends Function<Customer, ValidationResult> {

    enum ValidationResult{
        SUCCESS,
        PHONE_NUMBER_NOT_VALID,
        EMAIL_NOT_VALID,
        IS_NOT_ADULT
    }

    static CustomerRegistrationValidator isEmailValid(){
        return customer ->
                customer.getEMAIL().contains("@") ? ValidationResult.SUCCESS : ValidationResult.EMAIL_NOT_VALID;
    }

    static CustomerRegistrationValidator isPhoneNumberValid(){
        return customer ->
                customer.getPHONE_NUMBER().startsWith("+0") ? ValidationResult.SUCCESS : ValidationResult.PHONE_NUMBER_NOT_VALID;
    }

    static CustomerRegistrationValidator isAdult(){
        return customer ->
                Period.between(customer.getDob(), LocalDate.now()).getYears() > 18 ? ValidationResult.SUCCESS : ValidationResult.IS_NOT_ADULT;
    }

    /**
     *
     * @param other übergibt den nächsten Validator
     * @return CustomerRegistrationValidator Funktion
     */
    default CustomerRegistrationValidator and (CustomerRegistrationValidator other) {
        return customer -> {
            // this.apply(customer) bezieht sich auf die Funktion im Parameter die übergeben wird
            ValidationResult result = this.apply(customer);
            return result.equals(ValidationResult.SUCCESS) ? other.apply(customer) : result;
        };
    }



}
