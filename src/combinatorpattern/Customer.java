package combinatorpattern;

import java.time.LocalDate;

public class Customer {

    private final String NAME;
    private final String EMAIL;
    private final String PHONE_NUMBER;
    private final LocalDate dob;

    public Customer(String NAME, String EMAIL, String PHONE_NUMBER, LocalDate dob) {
        this.NAME = NAME;
        this.EMAIL = EMAIL;
        this.PHONE_NUMBER = PHONE_NUMBER;
        this.dob = dob;
    }

    public String getNAME() {
        return NAME;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public String getPHONE_NUMBER() {
        return PHONE_NUMBER;
    }

    public LocalDate getDob() {
        return dob;
    }
}
