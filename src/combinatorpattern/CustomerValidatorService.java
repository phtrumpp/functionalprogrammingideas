package combinatorpattern;

import java.time.LocalDate;
import java.time.Period;

public class CustomerValidatorService {

    private boolean isEmailValid(String email){
        return email.contains("@");
    }

    private boolean isPhoneNumberValid(String phoneNumber){
        return phoneNumber.contains("+0");
    }

    private boolean isAdult(LocalDate date){
        return Period.between(date,LocalDate.now()).getYears() > 16;
    }

    public boolean isValidCustomer(Customer customer){
        return isEmailValid(customer.getEMAIL()) && isPhoneNumberValid(customer.getPHONE_NUMBER())
                && isAdult(customer.getDob());
    }

}
