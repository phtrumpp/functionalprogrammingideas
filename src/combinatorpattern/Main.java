package combinatorpattern;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        Customer customer = new Customer(
                "Phil",
                "phtrumpp@gmail.com",
                "+01289173123",
                LocalDate.of(1993,7,23)
        );

        // System.out.println(new CustomerValidatorService().isValidCustomer(customer));


        CustomerRegistrationValidator.ValidationResult result = CustomerRegistrationValidator
                .isEmailValid()
                .and(CustomerRegistrationValidator.isPhoneNumberValid())
                .and(CustomerRegistrationValidator.isAdult())
                .apply(customer);


        System.out.println(result);
        if (result != CustomerRegistrationValidator.ValidationResult.SUCCESS){
            throw new IllegalStateException(result.name());
        }


    }
}
