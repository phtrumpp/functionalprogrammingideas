import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Function;

public class Main {

    public static void main(String[] args) {


        List<Student> studentList = Arrays.asList(

                new Student("Phil",26),
                new Student("Daniel",27),
                new Student("Zlatan",40),
                new Student("Xavier", 80)
        );

        // ToDo -- Drei verschiedene Arten , letzte implementiert lambdas (Arrow functions)

        // -- / --
        // siehe Pipikacka Klasse

        Collections.sort(studentList,new Class());

        // -- / --
        Collections.sort(studentList, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });

        // -- / --
        Collections.sort(studentList,(s1,s2) -> s1.getName().compareToIgnoreCase(s2.getName()));

        int [] a = {1,2,3};

        OptionalInt max = Arrays.stream(a).max();

        max.ifPresent(System.out::println);

        Integer integer = 2;


        Function<Integer,Integer> function = e -> e + 1/e;
        Function<Integer,Double> function2 = e -> (double)e + 1/(double)e;

        int out = function.apply(integer);

        double out2 = function2.apply(integer);


        System.out.println("Function integer return: " + out + " and as double return: " + out2);



    }



}
