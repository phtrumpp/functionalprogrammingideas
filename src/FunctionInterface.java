import java.util.function.BiFunction;
import java.util.function.Function;

public class FunctionInterface {
    public static void main(String[] args) {

        // Aufrufen der Funktion
        double a = functionA.apply(1);

        // Von einer anderen Klasse aus wie folgt (beachte statische Funktion):
        // double a = FunctionInterface.function.apply(x);

        double c = functionC.apply(1);

        double x = biFunction.apply(2,3);

        System.out.println("Variable a contains: " + a + " and c contains: " + c);
        System.out.println("Result of Bifunction x: " + (int)x);
    }


    /**
     * Als Ersatz für eine normale Methode verwende ich hier eine Funktion, mit einer hier definierten
     * Implementation.
     * Ich denke es macht überwiegend bei relativ simplen Aufgaben Sinn Funktionen anstelle von Methoden
     * zu verwenden.
     */
    static Function<Integer, Double> functionA = e -> (double)e/(e+(e*2));

    static Function<Double, Double> functionB = e -> e*2;


    /**
     * Kombination oder Verkettung von Funktionen klappt dank default Methode "andThen"
     * des funktionalen Interfaces wunderbar!
     * Zweite Funktion nimmt den Rückgabetypen der ersten Funktion als Argument.
     */
    static Function<Integer,Double> functionC = functionA.andThen(functionB);

    // BiFunction nimmt zwei Argumente an
    static BiFunction<Integer, Integer, Double> biFunction = (a,b) ->(double) a++ * b;

}
