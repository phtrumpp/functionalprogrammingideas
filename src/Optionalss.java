import java.util.Optional;

public class Optionalss {

    public static void main(String[] args) {

        Optional.ofNullable(null)
                .orElseGet(() -> "default value");

        Optional.ofNullable("Contains something")
                .orElseGet(() -> "default value");

        Optional.ofNullable("Hello")
                .orElseThrow(()-> new IllegalStateException("exception"));

        Optional.ofNullable("phtrumpp@gmail.com")
                .ifPresent(email -> System.out.println("Sending email to: " + email));

        System.out.println("----//----");

        // Nimmt zwei Argumente entgegen: Consumer (one args + void) und ein Runnable (no args+void)
        Optional.ofNullable("phtrumpp@htwg-konstanz.de")
                .ifPresentOrElse(System.out::println,() -> System.out.println("Contains no email!"));

        String adress = "Breitenfelder Str. 19";

        Optional<String> newAdress =  Optional.of(adress);


        System.out.println("----/Test Method/----");
        String nullString = null;

        String testString = testOptional(nullString);

        System.out.println(testString);

    }


    private static String testOptional(String test) {
        return Optional.ofNullable(test).orElseGet(()-> "Contains no adress");
    }
}
