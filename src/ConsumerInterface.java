import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerInterface {

    public static void main(String[] args) {


        consumer.accept(new Student("Philippe"));
        System.out.println("------");
        greetStudent(new Student("Philippe"));

        System.out.println("--BiConsumer--");
        consumerCondition.accept(new Student ("Jeanine"),true);
    }


    private static void greetStudent(Student student) {
        System.out.println("Hello " + student.getName() + " Welcome!");
    }

    private static Consumer<Student> consumer = s -> System.out.println("Hello " + s.getName() + " from Lambda");
    static BiConsumer<Student,Boolean> consumerCondition =
            (s,showName) -> System.out.println(showName ? "Hello " +s.getName():"Hello Customer");

}
