import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class Main2Test {

    @Test
    public void testReferences(){

        Student before = new Student("Daniel Würfel");

        List<Student> people = Stream.of(before).collect(Collectors.toList());

        // Same object with two references
        Student after = people.get(0);

        assertTrue(before==after);

        before.setName("Jeanine");

        // Same object, different reference
        assertEquals(before.getName(),after.getName());
    }

    @Test
    public void testConstructorReference() {
        Student before = new Student("Daniel Würfel");

        List<Student> studentList = new ArrayList<>();

        studentList = Stream.of(before)
                .map(Student::new) // funktioniert hier weil wir bei Student einen Konstruktor haben, der ein Objekt annimmt
                // siehe Student Klasse und entsprechender Konstruktor
                .collect(Collectors.toList());
    }

}