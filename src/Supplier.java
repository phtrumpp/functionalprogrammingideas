import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.DoubleSupplier;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Supplier {


    public static void main(String[] args) {



        DoubleSupplier doubleSupplier = new DoubleSupplier() {
            @Override
            public double getAsDouble() {
                return Math.random();
            }
        };

        doubleSupplier.getAsDouble();

        // as simple lambda
        DoubleSupplier doubleSupplier1 = () -> Math.random();
        doubleSupplier1.getAsDouble();

        // As method reference
        DoubleSupplier doubleSupplier2 = Math::random;
        doubleSupplier2.getAsDouble();


        // ------ ///

        List<String> names = Arrays.asList("Mal", "Wash", "Kaylee", "Inara",
                "Zoë", "Jayne", "Simon", "River", "Shepherd Book");

        Optional<String> first = names.stream()
                .filter(name -> name.startsWith("C"))
                .findFirst();

        System.out.println(first);
        System.out.println(first.orElse("None"));

        System.out.println(first.orElse(String.format("No result found in %s",
                names.stream().collect(Collectors.joining(", ")))));

        System.out.println(first.orElseGet(() ->
                String.format("No result found in %s",
                        names.stream().collect(Collectors.joining(", ")))));



        // ToDo --------------------------- ////

        Logger logger = Logger.getLogger("...");

        Student student = new Student("Philippe");

        logger.info(() -> "Hi I'm a returned by a supplier lambda");
        // Possible usage of a supplier getting a string from a object e.g.
        logger.info(() -> student.toString());
        logger.info(student::toString);

    }


}
