import com.sun.tools.javac.Main;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.DoubleSupplier;
import java.util.function.DoubleToLongFunction;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * The designers of Java could have decided these questions in several different ways. Prior to Java 8, the decision was not to allow static members in interfaces at all.
 * Unfortunately, however, that led to the creation of utility classes: classes that contain only static methods.
 * A typical example is java.util.Collections,
 * which contains methods for sorting and searching, wrapping collections in synchronized or unmodifiable types, and more.
 * In the NIO package, java.nio.file.Paths is another example.
 * It contains only static methods that parse Path instances from strings or URIs.
 * Now, in Java 8, you can add static methods to interfaces whenever you like.
 *
 * The requirements are: Add the static keyword to the method.
 *
 * Provide an implementation (which cannot be overridden).
 * In this way they are like default methods, and are included in the default tab in the Javadocs.
 *
 * Access the method using the interface name. Classes do not need to implement an interface to use its static methods.
 *
 * Kousen, Ken. Modern Java Recipes: Simple Solutions to Difficult Problems in Java 8 and 9 . O'Reilly Media. Kindle Edition.
 */
public class StaticMethodInterface {

    public static void main(String[] args) {



        List<String> names = Arrays.asList("Connery", "Lazenby", "Moore", "Dalton","Brosnan" ,"Craig");

        List<String> sortedNames = names.stream()
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
        // Comparator ist ein Interface mit default und statischen Methoden.
        // Klassen müssen nicht das Interface implementieren, von welchem sie statische Methoden verwenden

        System.out.println(sortedNames);

        // Sortieren nach Natürlicher Reihenfolge
        List<String> sortReverse = names.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());


        // Sortieren nach Kleinschreibung mit Comparator.compare (lambda)
        List<String> compareCase= names.stream()
                .sorted(Comparator.comparing(String::toLowerCase))
                .collect(Collectors.toList());

        // Sortieren nach String Länge mit Comparator.compare(lambda)
        List<String> compareLength = names.stream()
                .sorted(Comparator.comparing(String::length))
                .collect(Collectors.toList());


        //todo
        // Static methods in interfaces remove the need to create separate utility classes,
        // though that option is still available if a design calls for it. The key points to remember are:
        //
        // Static methods must have an implementation
        // You cannot override a static method
        // Call static methods from the interface name
        // You do not need to implement an interface to use its static methods


        System.out.println("------");
        List<Integer> a = Arrays.asList(1,2,4,2,3);
        a.sort(Collections.reverseOrder());
        a.forEach(System.out::println);
        System.out.println(a);




    }


}
