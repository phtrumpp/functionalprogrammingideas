import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Student {

    private String name;
    private int age;

    // Siehe unten Getter
    private Map<String, String> testMap;



    // Diesen Konstruktor wird nur für den Fall benötigt, bspw. falls ein neues Objekt in eine Liste (vom gleichen Typen)
    // gemappt werden soll (Klingt banal aber siehe MainTest2 - ConstructorReference Testmethode)
    public Student (Student student) {
        this.name = student.getName();
    }


    public Student(String name) {
        this.name = name;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    // Offtopic: Nicht modifizierbare Listen/Maps returnen für die Bewahrung von Enkapsulation
    public Map<String, String> getTestMap() {
        return Collections.unmodifiableMap(testMap);
    }

    //ToString nur mit Name(!), Alter absichtlich nicht hinzugefügt
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
