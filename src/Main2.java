import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main2 {

    public static void main(String[] args) {

        List<String> names = Arrays.asList(
                "Grace Hopper",
                "Barbara Liskov",
                "Ada Lovelace",
                "Karen Spärck Jones"
        );

        /**
         * JVM interpretiert Methoden Referenz Student::new in etwa so:
         * Für jeden Datentyp in der gestreamten Liste, erzeuge (map) ein Student Objekt und nimm den Namen (String)
         * als Argument für den Student Konstruktor (siehe Lambda im Kommentar)
         *
         * Funktioniert nur weil Student einen entsprechenden Konstruktor hat, ansonsten könnte die JVM nicht
         * richtig interpretieren.
         */
        List<Student> students = names.stream() //jedes Element in names ist ein String(Name)
                .map(Student::new)
                //.map(name -> new Student(name)) Beachte: Student Konstruktor nimmt einen String name als Argument
                .collect(Collectors.toList());


    }

}
